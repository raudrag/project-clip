package com.example.clip.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.clip.model.Client;

public interface ClientRepository  extends JpaRepository<Client, Long>{

}