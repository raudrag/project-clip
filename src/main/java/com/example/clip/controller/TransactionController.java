package com.example.clip.controller;

import java.util.List;

import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.clip.model.Client;
import com.example.clip.request.PaymentRequest;
import com.example.clip.service.PaymentService;

@RestController
@RequestMapping("/api/clip")
public class TransactionController {

	@Autowired
	private PaymentService service;

	@RequestMapping(value = "/createPayload", method = RequestMethod.POST)
	public ResponseEntity create(PaymentRequest paymentRequest) {

		try {
			/*
			 * paymentRepository.save(payment); log.info("Payload Created Successfully");
			 */
			return ResponseEntity.status(HttpStatus.OK).build();

		} catch (PersistenceException ex) {
			return ResponseEntity.status(HttpStatus.OK).body(ex.getMessage());
		}
	}

	@GetMapping
	public List<Client> userPayment() {
		return service.userPayment();
	}

	@GetMapping("/disbursement/process")
	public ResponseEntity disbursementProcess() {
		try {
			return ResponseEntity.ok(service.disbursementProcess());
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
	
	@GetMapping("/report/{id}")
	public ResponseEntity report(@RequestParam long id) {
	
		try {
			return ResponseEntity.ok(service.report(id));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}
}
