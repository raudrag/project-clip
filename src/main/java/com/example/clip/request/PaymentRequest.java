package com.example.clip.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentRequest {

    String userId;
    double amount;
    String status;
}