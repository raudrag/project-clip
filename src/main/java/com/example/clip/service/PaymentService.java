package com.example.clip.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.example.clip.model.Client;
import com.example.clip.model.response.ClientReport;
import com.example.clip.model.response.ClientsDisbursementProcess;
import com.example.clip.request.PaymentRequest;

public interface PaymentService {

	public ResponseEntity create(PaymentRequest paymentRequest);
	public List<Client> userPayment();
	public ClientsDisbursementProcess disbursementProcess();
	public ClientReport report(long id); 
	
}