package com.example.clip.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.clip.model.Client;
import com.example.clip.model.Payment;
import com.example.clip.model.response.ClientProcess;
import com.example.clip.model.response.ClientReport;
import com.example.clip.model.response.ClientsDisbursementProcess;
import com.example.clip.model.response.DisbursementProcess;
import com.example.clip.repository.ClientRepository;
import com.example.clip.repository.PaymentRepository;
import com.example.clip.request.PaymentRequest;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	PaymentRepository paymentRepository;

	@Autowired
	private ClientRepository clientRepo;

	@Override
	public List<Client> userPayment() {
		List<Client> listClient = new ArrayList<Client>();
		clientRepo.findAll().forEach(client -> {
			if (!client.getPayments().isEmpty())
				listClient.add(client);
		});
		return listClient;
	}

	@Override
	public ClientsDisbursementProcess disbursementProcess() {
		ClientsDisbursementProcess clientsDisbursementProcess = new ClientsDisbursementProcess();
		List<DisbursementProcess> clients = new ArrayList<DisbursementProcess>();

		DisbursementProcess disbursementProcess = new DisbursementProcess();
		ClientProcess listDisBurmentProcess = new ClientProcess();

		List<ClientProcess> listClientProcess = new ArrayList<ClientProcess>();

		try {
			// get clients
			clientRepo.findAll().forEach(client -> {
				log.info("get clients");
				disbursementProcess.setId(String.valueOf(client.getId()));
				disbursementProcess.setUser(client.getName());

				// obtaining processes from clients that have new status
				client.getPayments().forEach(process -> {
					log.info("obtaining processes from clients that have new status");
					if (process.getStatus().equals("NEW")) {
						double disbursement = process.getAmount() - (process.getAmount() * 0.035);
						listDisBurmentProcess.setPayment(process.getAmount());
						listDisBurmentProcess.setDisbursement(disbursement);
						listDisBurmentProcess.setStatus("PROCESSED");
						listClientProcess.add(listDisBurmentProcess);
						disbursementProcess.setProcess(listClientProcess);
						clients.add(disbursementProcess);

						// Update payment client
						log.info("Update payment client");
						Optional<Payment> updateProcess = paymentRepository.findById(client.getId());
						log.info("findById " + client.getId());
						updateProcess.get().setAmount(disbursement);
						updateProcess.get().setStatus("PROCESSED");
						paymentRepository.save(updateProcess.get());

					}
				});
				clientsDisbursementProcess.setClients(clients);
			});
			return clientsDisbursementProcess;
		} catch (Exception e) {
			throw new Error(e.getMessage());
		}
	}

	@Override
	public ResponseEntity create(PaymentRequest paymentRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ClientReport report(long id) {

		ClientReport clientReport = new ClientReport();

		try {
			Optional<Client> client = clientRepo.findById(id);
			String name = client.get().getName().concat(" ").concat(client.get().getSurname());
			clientReport.setUser_name(name);

			paymentRepository.findAll().forEach(payment -> {
				double payments_sum = clientReport.getPayments_sum();
				clientReport.setPayments_sum(payments_sum + payment.getAmount());
				
				if (payment.getStatus().equals("NEW")) {
					double new_payments = clientReport.getNew_payments();
					clientReport.setNew_payments(new_payments + payment.getAmount());
				}
				else {
					double new_payments_amount = clientReport.getNew_payments_amount();
					clientReport.setNew_payments_amount(new_payments_amount + (payment.getAmount()*0.035));
				}
				
			});
			return clientReport;
		} catch (Exception e) {
			throw new Error(e.getMessage());
		}
	}

}