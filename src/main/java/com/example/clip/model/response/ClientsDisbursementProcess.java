package com.example.clip.model.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ClientsDisbursementProcess {

	private List<DisbursementProcess> clients;
}