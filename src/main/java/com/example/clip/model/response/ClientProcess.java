package com.example.clip.model.response;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ClientProcess {

	private double payment;
	private double disbursement;
	private String status;
}