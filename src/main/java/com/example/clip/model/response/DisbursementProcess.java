package com.example.clip.model.response;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class DisbursementProcess {

	private String id;
	private String user;
	private List<ClientProcess> process;
	
}