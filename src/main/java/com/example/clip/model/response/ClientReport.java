package com.example.clip.model.response;

import lombok.Data;

@Data
public class ClientReport {

	private String user_name;
	private double payments_sum;
	private double new_payments;
	private double new_payments_amount;
}