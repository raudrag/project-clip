package com.example.clip;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.clip.model.Client;
import com.example.clip.model.Payment;
import com.example.clip.repository.ClientRepository;
import com.example.clip.repository.PaymentRepository;

@Component
public class Init {

	@Autowired
	private PaymentRepository repo;
	
	@Autowired
	private ClientRepository client;
	
	@PostConstruct
	public void initConfig() {

		Client cliente = new Client();
		Client cliente1 = new Client();
		Payment payment = new Payment();
		List<Payment> listPayment = new ArrayList<Payment>();
		
		cliente.setName("Raul");
		cliente.setSurname("Ortega");
		cliente.setPhone("1234567891");
		cliente.setAddress("CDMX");
		cliente.setEmail("raudrag@gmail.com");
		
		payment.setAmount(1240.09);
		payment.setStatus("NEW");
		payment.setClient(cliente);
		
		listPayment.add(payment);
		
		cliente.setPayments(listPayment);
		
		client.save(cliente);
		repo.save(payment);
		
		cliente1.setName("Alexis");
		cliente1.setSurname("Aguilar");
		cliente1.setPhone("9876542341");
		cliente1.setAddress("CDMX");
		cliente1.setEmail("ale@gmail.com");
	
		client.save(cliente1);
	}

}
